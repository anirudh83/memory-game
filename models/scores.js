module.exports = (sequelize, Sequelize) => {
        const Scores = sequelize.define("scores", {
                name: {
                        type: Sequelize.STRING        
                },
                email: {
                        type: Sequelize.TEXT,
                },
                bestscore: {
                        type: Sequelize.INTEGER     
                }     
        });
      
        return Scores;
};