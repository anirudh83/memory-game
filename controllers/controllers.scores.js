const store = require("store2");
const db = require("../models");
const store2 = require('store2');
const path = require('path');
const nodemailer = require("nodemailer");
const mailGun = require('nodemailer-mailgun-transport');
const Scores = db.scores;
const Op = db.Sequelize.Op;

async function main(userEmail, score) {

  const auth = {
    auth: {
        apiKey: "561fe496eb365ad8ab1f26cf01815dc0-b6d086a8-d25b3356",

        domain: "sandbox642e0f47dd8e46acb83b74113fda6e26.mailgun.org"
    }
  };

const transporter = nodemailer.createTransport(mailGun(auth));
  var info = {
    from: '"Anirudh Sharma " <ani838281@gmail.com>', 
    to: userEmail,
    subject: "Memory Game Score",
    text: "Your score is "+score,
    html: `Your score is ${score}`    
  };

  transporter.sendMail(info, function (err, data) {
    if (err) {
        console.log(err);
    }else{
      console.log(data);
    }
  });
 
}

function checkUserExits(data,userEmail){
    let isPresent = false;
    data.forEach(row => {
      if(row.email == userEmail){
        isPresent = true
      }
    })
    return isPresent
}

function getUserScore(data,userEmail){
  let userOldScore;
  let isPresent = false;
  data.forEach(row => {
    if(row.email == userEmail){
      userOldScore = row.bestscore;
    }
  })
  return userOldScore
}

exports.create = (req, res) => {
      let userScore;  
      Scores.findAll()
      .then(data => {
        return data;
      })
      .then(data=>{
        if(checkUserExits(JSON.parse(JSON.stringify(data)),req.body.email) == false){
          console.log("creating new user")
          score = {
            name : req.body.name,
            email : req.body.email,
            bestscore : req.body.scoreValue
          }
          Scores.create(score)
          .then(data => {
            console.log("Score created")
          })
          .catch(err => {
            res.status(500).send({
              message:
                err.message || "Some error occurred while creating the Score."
            });
          });
        }else{
          if(getUserScore(data,req.body.email) > req.body.scoreValue){
            score = {
              name : req.body.name,
              email : req.body.email,
              bestscore : req.body.scoreValue
            }
            console.log("Updating")
            Scores.update(score, {
              where: { email: req.body.email }
            })
              .then(num => {
                if (num == 1) {
                  console.log("Score Updated")
                } else {
                  console.log(`Cannot update score  with id=${req.body.email}. Maybe Score was not found or req.body is empty!`)
                }
              })
              .catch(err => {
                console.log(err.message)
              });
          }
        }
        main(req.body.email,req.body.scoreValue);
        res.status(204).send().end();
      })
      .catch(err => {
        res.status(500).send({
          message: err.message ||"Error retrieving score all score "
        });
      });
};      


exports.getTop5 = (req, res) => {
  Scores.findAll({ 
    order: [
      ['bestscore', 'ASC'], 
    ],
    limit :5
  })
  .then(data => {
    res.send(data).status(200).end();
  })
  .catch(err => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while retrieving Scores."
    }).end();
  });
}