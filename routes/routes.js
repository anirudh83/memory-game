const express = require('express');
const bodyParser = require('body-parser');
const score = require('../controllers/controllers.scores.js');

const route = express.Router();
var jsonParser = bodyParser.json()
 
var urlencodedParser = bodyParser.urlencoded({ extended: false });

route.get('/',(req,res)=>{
        res.sendFile(path.join(__dirname,"../index.html"));
})

route.post('/score', urlencodedParser, score.create);

route.get('/all', score.getTop5);

module.exports = route;