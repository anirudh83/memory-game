function createTopScore(){
        let tbody = document.getElementsByTagName("tbody");
        fetch('/all')
        .then(response => response.json())
        .then(data => {
          let table = document.querySelector("tbody");
          data.forEach(row=>{
            let tr = document.createElement("tr");
            let tdname = document.createElement("td");
            let tdscore = document.createElement("td");
            let tddate = document.createElement("td");
            tdname.textContent = row["name"];
            tdscore.textContent = row["bestscore"];
            tddate.textContent = row["createdAt"].slice(8,10)+"/"+row["createdAt"].slice(5,7)+"/"+row["createdAt"].slice(0,4);
            tr.appendChild(tdname);
            tr.appendChild(tdscore);
            tr.appendChild(tddate);
            table.appendChild(tr);
          }) 
        })
        .catch(err => console.log(err));
}

createTopScore();