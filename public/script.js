const gifNumber = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

let winnerCheck = false;
let count = 0;
let firstHit = "";
let secondHit = "";
let firstEvent, secondEvent;
let freeze = true;
let completeMatch = [];
let clickedDiv = new Set();
let localScore;
let isNameValid, isEmailValid;

let gameContainer = document.getElementById("game");
let reset = document.querySelector(".reset");
let lowestScore = document.querySelector(".lowest span");
let countSpan = document.querySelector(".count span");
let unameSelector = document.querySelector("#uname");
let emailSelector = document.querySelector("#email");
let errorUname = document.querySelector(".errorUname");
let submitEnabled = document.querySelector("#submitbtn");
let scoreValue = document.querySelector("#scoreValue");
let scoreForm = document.querySelector("#scoreForm");
let winner = document.querySelector(".winner");

function shuffle(array) {
  let counter = array.length;
  for (var i = array.length - 1; i > 0; i--){
    var j = Math.floor(Math.random() * (i + 1));
    var temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
}

function createDivsForGif(gifArray) {
  gifArray.forEach((gif) => {
    const scene = document.createElement("div");
    const card = document.createElement("div");
    const card__face__front = document.createElement("div");
    const card__face__back = document.createElement("div");

    scene.classList.add("scene");
    card.classList.add("card", "gifFront" + gif);
    card__face__front.classList.add(
      "gif" + gif,
      "card__face",
      "card__face--front"
    );
    card__face__back.classList.add("card__face", "card__face--back");
    card__face__back.style.backgroundImage = "url(gifs/" + (gif % 6) + ".gif)";

    scene.addEventListener("click", handleCardClick);
    card.append(card__face__back);
    card.append(card__face__front);
    scene.append(card);
    gameContainer.append(scene);
  });
}

// handle click event on flip card
function handleCardClick(event) {
  let gifNumber;
  let divGifParent;
  if (firstEvent != event.target.parentElement) {
    if (event.target.classList[0].slice(0, 3) == "gif") {
      divGifParent = event.target.parentElement;
      gifNumber = getGifNumber(event.target.classList[0]);
      if (reset.style.visibility == "" || reset.style.visibility == "hidden") {
        reset.style.visibility = "visible";
        reset.classList.add("animate__bounceInDown");
      }
  
      if (!completeMatch.includes(gifNumber)) {
        if (firstHit.length == 0 && freeze) {
          firstHit = gifNumber;
          firstEvent = event.target.parentElement;
          firstEvent.classList.add("is-flipped");
          count += 1;
          clickedDiv.add(divGifParent.classList[1]);
        } else if (freeze == true) {
          secondHit = gifNumber;
          secondEvent = event.target.parentElement;
          if (secondHit == firstHit) {
            completeMatch.push(gifNumber);
            secondEvent.classList.add("is-flipped");
            firstHit = "";
            firstEvent = "";
            if (completeMatch.length == 6) {
              winnerCheck = true;
            }
          } else {
            freeze = false;
            secondEvent.classList.add("is-flipped");
            setTimeout(() => {
              firstEvent.classList.remove("is-flipped");
              secondEvent.classList.remove("is-flipped");
              firstHit = "";
              secondHit = "";
              freeze = true;
            }, 1000);
          }
          count += 1;
          clickedDiv.add(divGifParent.classList[1]);
        }
      }
      countSpan.innerHTML = count;
  
      if (winnerCheck) {
        winner.style.display = "block";
        if (
          localStorage.getItem("lowestScore") > count ||
          localStorage.getItem("lowestScore") == null
        ) {
          lowestScore.innerHTML = count;
          localStorage.setItem("lowestScore", count);
        } else {
          lowestScore.innerHTML = localStorage.getItem("lowestScore");
        }
        if (localStorage.getItem("email") == null) {
          scoreForm.style.display = "block";
        } else {
          let email = document.querySelector("#email");
          let name = document.querySelector("#name");
          email.value = localStorage.getItem("email");
          name.value = localStorage.getItem("uname");
          scoreValue.value = count;
          submitEnabled.removeAttribute("disabled");
          scoreForm.submit();
        }
      }
    }
  }
}

//validate user name
function validateUname() {
  const uname = document.userScore.name.value;
  const email = document.userScore.email.value;
  isNameValid = false;
  if(isNameValid == false|| isNameValid == false){
    submitEnabled.disabled = true;
  }
  if (/\d/.test(uname)) {
    errorUname.textContent = "Remove number from name";
    errorUname.style.display = "block";
  }else if (/\W/.test(uname)) {
    errorUname.textContent = "Remove special character from name";
    errorUname.style.display = "block";
  }else if (uname.length == 0) {
    errorUname.textContent = "Name can not be empty";
    errorUname.style.display = "block";
  }else if (uname.length >=15) {
    errorUname.textContent = "Name length should not greater than 15";
    errorUname.style.display = "block";
  }else {
    errorUname.style.display = "none";
    isNameValid = true;
    if (isNameValid && isEmailValid) {
      localStorage.setItem("email", email);
      localStorage.setItem("uname", uname);
      if (localStorage.getItem("lowestScore") > count) {
        localStorage.setItem("lowestScore", count);
      }
      scoreValue.value = count;
      submitEnabled.removeAttribute("disabled");
    }
  }
}

//validate Email
function validateEmail() {
  const email = document.userScore.email.value;
  const uname = document.userScore.name.value;
  isEmailValid = false;
  let errorEmail = document.querySelector(".errorEmail");
  const mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

  if(isNameValid == false|| isNameValid == false){
    submitEnabled.disabled = true;
  }
  if (!mailformat.test(email)) {
    errorEmail.textContent = "Invalid email address";
    errorEmail.style.display = "block";
  }else {
    isEmailValid = true;
    errorEmail.style.display = "none";

    if (isNameValid && isEmailValid) {
      localStorage.setItem("email", email);
      localStorage.setItem("uname", uname);
      if (localStorage.getItem("lowestScore") > count) {
        localStorage.setItem("lowestScore", count);
      }
      scoreValue.value = count;
      submitEnabled.removeAttribute("disabled");
    }
  }
}

//find image number from class name
function getGifNumber(gif) {
  let gifslice;
  if (gif.length == 5) {
    gifslice = gif.match(/(\d+)/);
  } else {
    gifslice = gif.match(/(\d+)/);
  }
  let gifNumber = parseInt(Object.values(gifslice)[0]) % 6;
  return gifNumber;
}

// if score is not sent in local storage then set the score in local storage
function checkLocalStorage() {
  if (localStorage.getItem("lowestScore") == null) {
    lowestScore.parentElement.style.display = "none";
  } else {
    lowestScore.parentElement.style.display = "block";
    lowestScore.innerHTML = localStorage.getItem("lowestScore");
    localScore = localStorage.getItem("lowestScore");
  }
}


let shuffledGifNumber = shuffle(gifNumber);
createDivsForGif(shuffledGifNumber);
checkLocalStorage();

// reset all variable 
reset.addEventListener("click", (e) => {
  count = 0;
  freeze = true;
  countSpan.innerHTML = count;
  firstHit = "";
  secondHit = "";
  winnerCheck = false;
  let winner = document.querySelector(".winner");
  firstEvent = "";
  secondEvent = "";

  if (winner.style.display == "block") {
    winner.style.display = "none";
  }
  clickedDiv.forEach((gifDiv) => {
    divList = document.querySelector("." + gifDiv);
    if (divList.classList.contains("is-flipped")) {
      divList.classList.remove("is-flipped");
    }
  });

  reset.classList.add("animate__bounceOutUp");
  completeMatch = [];
  location.reload();
});

submitEnabled.addEventListener("click", function () {
  scoreForm.style.display = "none";
});
