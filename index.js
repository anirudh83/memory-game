const express = require('express');
const path = require('path');
const bodyParser = require("body-parser");
const taskRoutes = require('./routes/routes.js');
const db = require('./models/index.js');
const store = require('store2');


const app = express();


app.use(express.static(path.join(__dirname, './public')));

app.use('/',taskRoutes);
db.sequelize.sync();

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});